import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewExpensePagePage } from './new-expense-page.page';

const routes: Routes = [
  {
    path: '',
    component: NewExpensePagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewExpensePagePage]
})
export class NewExpensePagePageModule {}
