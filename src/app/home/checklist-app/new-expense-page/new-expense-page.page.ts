import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-expense-page',
  templateUrl: './new-expense-page.page.html',
  styleUrls: ['./new-expense-page.page.scss'],
})
export class NewExpensePagePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
