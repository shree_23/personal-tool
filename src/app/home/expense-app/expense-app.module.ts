import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExpenseRoutingModule } from './expenses-routing.module';
import { IonicModule } from '@ionic/angular';
import { ExpenseAppPage } from './expense-app.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpenseRoutingModule
  ],
  declarations: [ExpenseAppPage]
})
export class ExpenseAppPageModule {}
