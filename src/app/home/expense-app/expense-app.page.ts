import { Component, OnInit } from '@angular/core';
import { EachExpenseListItem } from '../../model/expenses.model';
import { SharedServicesService } from '../../services/shared-services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-expense-app',
  templateUrl: './expense-app.page.html',
  styleUrls: ['./expense-app.page.scss'],
})
export class ExpenseAppPage implements OnInit {

  allExpenses: EachExpenseListItem;
  constructor(private sharedServices: SharedServicesService, public route: Router) {
   }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.allExpenses = this.sharedServices.getAllExpensesFromLocalDB();
  }

  createNewExpense() {
    delete window.localStorage.expenses;
    this.route.navigate(['/home/expenses/new-expense']);
  }

  deleteThisExpense(expense) {
    this.sharedServices.deleteItemfromDb(expense);
    const options = {
      messageToDisplay: 'Expenses deleted',
      duration: 2000,
      position: 'middle',
      color: 'dark'
    };
    this.sharedServices.showToastMessage(options).then(() => {
      this.ionViewDidEnter();
    });
  }

  editExistingExpense(expense) {
    this.route.navigate(['/home/expenses/new-expense'], { queryParams : {id: expense.id}});
  }

}
