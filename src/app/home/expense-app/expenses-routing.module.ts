import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpenseAppPage } from './expense-app.page';


const routes: Routes = [
    {
        path: '',
        component: ExpenseAppPage
    },
    {
        path: 'new-expense',
        loadChildren: './new-expense-page/new-expense-page.module#NewExpensePagePageModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ]
})
export class ExpenseRoutingModule {}
