import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExpenseModalPagePage } from './expense-modal-page.page';

const routes: Routes = [
  {
    path: '',
    component: ExpenseModalPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExpenseModalPagePage]
})
export class ExpenseModalPagePageModule {}
