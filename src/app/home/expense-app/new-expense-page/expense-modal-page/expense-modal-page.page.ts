import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-expense-modal-page',
  templateUrl: './expense-modal-page.page.html',
  styleUrls: ['./expense-modal-page.page.scss'],
})
export class ExpenseModalPagePage implements OnInit {
  public spentFor: string;
  public amount: number;
  public paymentMode: string;
  public type: string;

  @Input() expenseSpentFor: string;
  @Input() expenseAmount: number;
  @Input() id: number;
  @Input() modeOfPayment: string;
  @Input() modeType: string;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  close() {
    this.modalCtrl.dismiss();
  }
  submitExpense() {
    if (this.paymentMode === 'Credit Card' || this.paymentMode === 'Debit Card') {
      this.type = 'card';
    } else {
      this.type = 'cash';
    }
    this.modalCtrl.dismiss({
      expenseSpentFor : this.spentFor,
      expenseAmount : this.amount,
      modeOfPayment : this.paymentMode,
      type: this.type,
      id : this.id
    }, 'expenseSubmit');
  }
}
