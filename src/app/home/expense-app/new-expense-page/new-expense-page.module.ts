import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {ExpenseModalPagePage} from './expense-modal-page/expense-modal-page.page'

import { IonicModule } from '@ionic/angular';

import { NewExpensePagePage } from './new-expense-page.page';

const routes: Routes = [
  {
    path: '',
    component: NewExpensePagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewExpensePagePage, ExpenseModalPagePage],
  entryComponents: [ExpenseModalPagePage]
})
export class NewExpensePagePageModule {}
