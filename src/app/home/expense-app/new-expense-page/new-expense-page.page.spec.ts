import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewExpensePagePage } from './new-expense-page.page';

describe('NewExpensePagePage', () => {
  let component: NewExpensePagePage;
  let fixture: ComponentFixture<NewExpensePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewExpensePagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewExpensePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
