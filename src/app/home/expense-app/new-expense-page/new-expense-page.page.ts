import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, ToastController, NavController } from '@ionic/angular';
import { ExpenseModalPagePage } from './expense-modal-page/expense-modal-page.page';
import { SharedServicesService } from '../../../services/shared-services.service';
import { Expenses, EachExpenseListItem } from '../../../model/expenses.model';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-new-expense-page',
  templateUrl: './new-expense-page.page.html',
  styleUrls: ['./new-expense-page.page.scss'],
})
export class NewExpensePagePage implements OnInit, OnDestroy {

  expenseList: EachExpenseListItem;
  expensesCache: Expenses;
  expenseName = `${moment().format('MMM DD')} expenses`;

  /* displayIncomeField = false;
  incomeIndicator = false;
  incomebtnText = 'Add Income';
  incomeValue = 0; */
  showEdit: boolean;
  enabledToEdit: boolean;
  totalexpense: number;

  constructor(public modalController: ModalController, public toastController: ToastController,
              public sharedService: SharedServicesService, public activatedRoute: ActivatedRoute,
              public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.showEdit = false;
    this.enabledToEdit = true;
    this.expensesCache = {
      id: '',
      expenses: [],
      expenseName: this.expenseName,
      totalExpenses: 0
    };
    if (this.sharedService.getExpensesFromLocalDB()) {
      this.expensesCache = this.sharedService.getExpensesFromLocalDB();
    }
  }

  ionViewWillEnter() {
    let selectedExpenseById;
    this.activatedRoute.queryParams.subscribe(data => {
      if (data && data.id) {
        const localDBdata = this.sharedService.getAllExpensesFromLocalDB();
        if (localDBdata) {
          selectedExpenseById = localDBdata.listItems.find(a => a.id === data.id);
          this.expensesCache = {
            id: data.id,
            expenses: selectedExpenseById.expenses,
            expenseName: selectedExpenseById.expenseName,
            totalExpenses: selectedExpenseById.totalexpense
          };
          this.expenseName = selectedExpenseById.expenseName;
          this.showEdit = true;
          this.enabledToEdit = false;
        }
      }
    });
    this.showTotalAmount();
  }

  ionViewDidLeave() {
    console.log('left');
    this.clearAll();
    this.showEdit = false;
  }

  async addNewExpense(data = {}) {
    const modal = await this.modalController.create({
      component: ExpenseModalPagePage,
      componentProps : data
    });
    await modal.present();
    const expense = await modal.onDidDismiss();
    if (expense && expense.data) {
      if (expense.data.id > -1) {
        const id = expense.data.id;
        this.expensesCache.expenses[id].spentFor = expense.data.expenseSpentFor;
        this.expensesCache.expenses[id].amount = expense.data.expenseAmount;
        this.expensesCache.expenses[id].modeOfPayment = expense.data.modeOfPayment;
        this.expensesCache.expenses[id].type = expense.data.type;
      } else {
        this.expensesCache.expenses.push({
          spentFor: expense.data.expenseSpentFor,
          amount: expense.data.expenseAmount,
          modeOfPayment : expense.data.modeOfPayment,
          type : expense.data.type
        });
      }
      /* if (this.incomeValue) {
        this.incomeValue -= expense.data.expenseAmount;
      } */
      this.showTotalAmount();
      this.sharedService.saveExpensesToLocalDB(JSON.stringify(this.expensesCache));
    }
  }

  async save() {
    this.expensesCache.expenseName = this.expenseName;
    this.expensesCache.totalExpenses = this.totalexpense;
    if (this.expensesCache.id) {
      this.updateExistingExpense(this.expensesCache);
    } else {
      this.expensesCache.id = Math.random().toString(36).substring(7);
      const eachExpenses: Array<Expenses> = [];
      eachExpenses.push(this.expensesCache);
      let existingDatafromDB = this.sharedService.getAllExpensesFromLocalDB();
      if (!existingDatafromDB) {
        this.expenseList = {
          listItems: eachExpenses
        };
        existingDatafromDB = this.expenseList;
      } else {
        existingDatafromDB.listItems.push(eachExpenses[0]);
      }
      this.sharedService.saveAllexpensesToLocalDB(existingDatafromDB);
    }
    const options = {
      messageToDisplay: 'Expenses have been saved successfully',
      duration: 2000,
      position: 'middle',
      color: 'dark'
    };
    this.sharedService.showToastMessage(options).then(() => {
      this.navCtrl.navigateBack('home/expenses');
    });
  }

  showTotalAmount() {
    this.totalexpense = 0;
    this.expensesCache.expenses.forEach(eachItem => {
      this.totalexpense += eachItem.amount;
    });
    this.expensesCache.totalExpenses = this.totalexpense;
  }

  updateExistingExpense(expenseToBeUpdated) {
    this.sharedService.updateItemById(expenseToBeUpdated.id, expenseToBeUpdated);
  }

  editThisExpense(expense, index) {
    expense.id = index;
    this.addNewExpense(expense);
  }
  deleteThisItem(index) {
    this.expensesCache.expenses = this.expensesCache.expenses.filter( (cache, i) => i !== index);
    this.showTotalAmount();
  }

  clearAll() {
    console.log('deleting from local storage....');
    this.sharedService.deleteFromLocalDB();
    this.ngOnInit();
    /* this.resetIncomeField(); */
  }
  /* showIncomeField() {
    if (!this.displayIncomeField && !this.incomeIndicator) {
      this.displayIncomeField = true;
      this.incomebtnText = 'Remove Income';
    } else {
      this.resetIncomeField();
    }
  } */

  /* addIncomeToExpense(incomeValue) {
    this.incomeIndicator = true;
    this.displayIncomeField = false;
    this.expensesCache.expenses.map(expense => this.incomeValue -= expense.amount);
  } */

  edit() {
    this.showEdit = false;
    this.enabledToEdit = true;
  }

  /* resetIncomeField() {
    this.displayIncomeField = false;
    this.incomeIndicator = false;
    this.incomebtnText = 'Add Income';
    this.incomeValue = 0;
  } */
  ngOnDestroy() {
    console.log('page destroyed');
  }
}
