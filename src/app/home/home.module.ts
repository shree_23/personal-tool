import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
      },
      {
        path: 'expenses',
        loadChildren: './expense-app/expense-app.module#ExpenseAppPageModule'
      },
      {
        path: 'checklist',
        loadChildren: './checklist-app/checklist-app.module#ChecklistAppPageModule'
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
