import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router, ) {}

  launchModule(selectedModule) {
    const openModule = {
      expense : () => { this.route.navigateByUrl('/home/expenses'); },
      checkList : () => { this.route.navigateByUrl('/home/checklist');  }
    };
    openModule[selectedModule]();
  }
}
