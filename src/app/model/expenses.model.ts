export interface Expenses {
    id: string;
    expenseName?: string;
    totalExpenses: number;
    expenses: Array<ExpenseItems>;
}

export interface ExpenseItems {
    spentFor: string;
    amount: number;
    modeOfPayment: string;
    type: string;
}

export interface AllExpenses {
    expensesList: EachExpenseListItem;
}

export interface EachExpenseListItem {
    listItems: Array<Expenses>;
}