import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SharedServicesService {

  constructor(public toastController: ToastController) { }

  saveExpensesToLocalDB(expenses) {
    window.localStorage.setItem('expenses', expenses);
  }

  getExpensesFromLocalDB() {
    return JSON.parse(window.localStorage.getItem('expenses'));
  }

  saveAllexpensesToLocalDB(allExpensesToBeSaved) {
    window.localStorage.setItem('allexpenses', JSON.stringify(allExpensesToBeSaved));
  }

  getAllExpensesFromLocalDB() {
    return JSON.parse(window.localStorage.getItem('allexpenses'));
  }

  deleteItemfromDb(itemToBeDeleted) {
    const cache = this.getAllExpensesFromLocalDB();
    cache.listItems = cache.listItems.filter( cacheItem => cacheItem.id !== itemToBeDeleted.id);
    this.saveAllexpensesToLocalDB(cache);
  }
  updateItemById(id, expDataObj) {
    const allexpenses = this.getAllExpensesFromLocalDB();
    allexpenses.listItems.map(list => {
      if (list.id === id) {
        Object.assign(list, expDataObj);
      }
    });
    this.saveAllexpensesToLocalDB(allexpenses);
  }
  deleteFromLocalDB() {
    delete window.localStorage.expenses;
  }
  async showToastMessage(options) {
    const toast = await this.toastController.create({
      message: options.messageToDisplay,
      duration: options.duration,
      position: options.position,
      color: options.color
    });
    return toast.present();
  }
}
